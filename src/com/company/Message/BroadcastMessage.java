package com.company.Message;

public class BroadcastMessage extends Message {
    private int sourceId;

    public BroadcastMessage(Object payload, int sourceId, int stamping) {
        super(payload, stamping);
        this.sourceId = sourceId;
    }

    public int getSourceId()
    {
        return sourceId;
    }
}
