package com.company.SystemMessage;

public class SynchronizedMessage {
    private int sourceId;

    public SynchronizedMessage(int sourceId) {
        this.sourceId = sourceId;
    }

    public int getSourceId(){
        return sourceId;
    }
}
